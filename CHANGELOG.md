# CHANGELOG



## v0.2.1 (2024-04-08)

### Fix

* fix(deploy): add deploy pipeline ([`e43e0b0`](https://gitlab.com/federicoduca/my-package/-/commit/e43e0b094a3919c720e850f85524e0ac22022343))


## v0.2.0 (2024-04-08)

### Feature

* feat(test): add new test module ([`8c53cd7`](https://gitlab.com/federicoduca/my-package/-/commit/8c53cd7943b32c4d1f9dc49463a04f8b87a71daa))


## v0.1.0 (2024-04-08)

### Feature

* feat(semantic-release): add semantic release pipeline ([`8b2e7b1`](https://gitlab.com/federicoduca/my-package/-/commit/8b2e7b1bd6b2e511325562383d469d27c324632b))

### Unknown

* Add python-semantic-release config ([`83c3a38`](https://gitlab.com/federicoduca/my-package/-/commit/83c3a38710972fe674bd7b69a448ac8c878afc2b))

* Add python-semantic-release to dev group dependency in poetry ([`5eb67bc`](https://gitlab.com/federicoduca/my-package/-/commit/5eb67bc770f67bb3cbb1a05f54c9a9c4e1c55214))

* Add pyproject.toml to init poetry ([`0e5b87f`](https://gitlab.com/federicoduca/my-package/-/commit/0e5b87f51ffb7eb0beba3b3af04fae58307dc94d))

* Update first CI/CD pipeline file ([`667c34d`](https://gitlab.com/federicoduca/my-package/-/commit/667c34dcbd1c345e2f9df2ce833e34064bb1ebf7))

* Add first CI/CD pipeline yaml file ([`81b07dd`](https://gitlab.com/federicoduca/my-package/-/commit/81b07dd5e73ee71d08a353616cd88b516246a6a7))

* Initial commit ([`6433597`](https://gitlab.com/federicoduca/my-package/-/commit/643359700bad5e7a54245bd72ee7a40aedab8afd))
